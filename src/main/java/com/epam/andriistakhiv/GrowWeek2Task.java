package com.epam.andriistakhiv;
import java.util.Arrays;
import java.util.Scanner;

//Завдання з https://grow.telescopeai.com/

public class GrowWeek2Task {
    public void task2() {
        Scanner scanner = new Scanner(System.in);
        StringBuilder sb = new StringBuilder();

        System.out.println("Type your text:");
        String userInput = scanner.nextLine();

        char [] arc = new GrowWeek2Task().arraySort(userInput);

        for (int i = 0; i < arc.length; i++) {
            System.out.println(sb.append(Character.toString(arc[i])));
        }
    }

    private char[] arraySort (String s) {
        char[] inputChars = s.toCharArray();
        Arrays.sort(inputChars);
        return inputChars;
    }
}